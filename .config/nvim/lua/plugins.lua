-- vim:fdm=marker:

vim.opt.rtp:prepend(vim.fn.stdpath("data") .. "/lazy/lazy.nvim")

-- {{{ lualine config
local lualineconfig = {
    options = {
        theme = 'base16'
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {{'filename', path = 1}},
        lualine_c = {'branch', 'diff'},
        lualine_x = {},
        lualine_y = {'filetype', 'progress'},
        lualine_z = {'location'}
    }
}
 --- }}}

-- {{{ treesitter config
local function treesitterconfig()
    require('nvim-treesitter.configs').setup {
        sync_install = false,
        auto_install = true,

        context_commentstring = {
            enable = true
        },
        autopairs = {
            enable = true
        },
        matchup = {
            enable = true
        },
        highlight = {
            enable = true,
        },
        incremental_selection = {
            enable = true,
            keymaps = {
                init_selection = "gnn",
                node_incremental = "grn",
                scope_incremental = "grc",
                node_decremental = "grm",
            },
        },
        indent = {
            enable = true
        },
        refactor = {
            navigation = {
                enable = true,
                keymaps = {
                    goto_definition = "gnd",
                    list_definitions = "gnD",
                    list_definitions_toc = "gO",
                    goto_next_usage = "<a-*>",
                    goto_previous_usage = "<a-#>",
                },
            },
            smart_rename = {
                enable = true,
                keymaps = {
                    smart_rename = "grr",
                },
            },
            highlight_current_scope = { enable = false },
            highlight_definitions = { enable = false },
        },
        textobjects = {
            select = {
                enable = true,
                keymaps = {
                    ["af"] = "@function.outer",
                    ["if"] = "@function.inner",
                    ["ac"] = "@class.outer",
                    ["ic"] = "@class.inner",
                    ["aa"] = "@parameter.outer", -- an /a/rgument
                    ["ia"] = "@parameter.inner",
                },
            },
            swap = {
                enable = true,
                swap_next = {
                    ["<leader>a"] = "@parameter.inner",
                },
                swap_previous = {
                    ["<leader>A"] = "@parameter.inner",
                },
            },
        },
    }
end -- }}}

require('lazy').setup({
    -- treesitter
    { "nvim-treesitter/nvim-treesitter",
        config = treesitterconfig,
        build = ':TSUpdate'
    },

    -- telescope
    { "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            { "nvim-telescope/telescope-fzf-native.nvim",
                config = function() require('telescope').load_extension('fzf') end,
                build = "make",
            },
        },
        branch = "0.1.x",
        keys = {
            { "<leader>f", function() require("telescope.builtin").builtin() end, desc = "Telescope pickers" },
            { "<leader>ff", function() require("telescope.builtin").find_files() end, desc = "Fuzzy find files" },
            { "<leader>fg", function() require("telescope.builtin").live_grep() end, desc = "Fuzzy grep" },
            { "<leader>fb", function() require("telescope.builtin").buffers() end, desc = "Fuzzy find buffers" },
            { "<leader>ft", function() require("telescope.builtin").tags() end, desc = "Fuzzy find tags" },
        }
    },

    -- editing support
    { "nvim-treesitter/nvim-treesitter-textobjects",
        dependencies = 'nvim-treesitter/nvim-treesitter'
    },
    { "nvim-treesitter/nvim-treesitter-refactor",
        dependencies = 'nvim-treesitter/nvim-treesitter',
    },
    { "kylechui/nvim-surround",
        version = "*",
        event = "VeryLazy",
        config = true
    },
    { "windwp/nvim-autopairs",
        opts = { check_ts = true },
        enabled = false
    },
    { "numToStr/Comment.nvim",
        config = true,
        lazy = false
    },
    { "Wansmer/treesj",
        keys = { '<space>m', '<space>j', '<space>s' },
        dependencies = { 'nvim-treesitter/nvim-treesitter' },
        config = true
    },
    {'kevinhwang91/nvim-bqf'},

    -- movement
    { "ggandor/leap.nvim",
        config = function() require('leap').add_default_mappings() end,
        enabled = false
    },

    -- visual
    { "NvChad/nvim-colorizer.lua",
        config = true,
        ft = {"css", "html", "javascript"}
    },
    { "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        opts = {
            indent = {
                char = '▏'
            },
            scope = {
                enabled = false,
            }
        },
        init = function() 
            local hooks = require "ibl.hooks"
            hooks.register(
                hooks.type.WHITESPACE,
                hooks.builtin.hide_first_space_indent_level
            )
        end

    },
    { "psliwka/vim-smoothie", enabled = false },
    { "andymass/vim-matchup",
        enabled = false,
    },
    { "lewis6991/gitsigns.nvim", 
        opts = { numhl = false }
    },
    { "RRethy/nvim-base16",
        config = function() require('base16-colorscheme').setup{
                base00 = '#181818', base01 = '#282a2e', base02 = '#373b41', base03 = '#969896',
                base04 = '#b4b7b4', base05 = '#c5c8c6', base06 = '#e0e0e0', base07 = '#ffffff',
                base08 = '#cc342b', base09 = '#f96a38', base0A = '#fba922', base0B = '#198844',
                base0C = '#3971ed', base0D = '#3971ed', base0E = '#a36ac7', base0F = '#3971ed'
            } end,
        lazy = false,
        priority = 1000,
    },
    { "nvim-lualine/lualine.nvim", opts = lualineconfig },
    { "neovim/nvim-lspconfig",
        enabled = false,
        config = function()
            -- require'lspconfig'.clangd.setup{}
            require'lspconfig'.tsserver.setup{}
        end,
    }
},
{
ui = {
    border = "rounded",
    icons = {
        cmd = "⌘",
        config = "🛠",
        event = "📅",
        ft = "📂",
        init = "⚙",
        keys = "🗝",
        plugin = "🔌",
        runtime = "💻",
        source = "📄",
        start = "🚀",
        task = "📌",
        lazy = "💤 ",
    },
},
})
