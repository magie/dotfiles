set number relativenumber " enable line numbers
set splitbelow splitright " sane splitting
set expandtab tabstop=4 softtabstop=4 shiftwidth=4 " fix the way tabs work
set noshowmode
set smartcase ignorecase
set undofile
set hidden " switch buffers without saving
set inccommand=nosplit
set virtualedit=block
set termguicolors
set foldcolumn=1
set scrolloff=1
set timeoutlen=500
set mouse+=a " enable mouse support
set clipboard=unnamedplus " copy/paste from * reg
set shortmess+=I
let g:netrw_home = '~/.local/share/netrw/'

au TextYankPost * silent! lua vim.highlight.on_yank{timeout=250}
let mapleader = " " " leader is space

lua require('plugins')

" MAPS
nmap <leader>o o<Esc>
nmap <leader>O O<Esc>
nmap <silent> <leader>l <cmd>noh<cr>

" ABBREVIATIONS
iab flase false
iab Flase False
iab slef self

" SYNTAX
syntax keyword Todo contained TODO FIXME XXX

