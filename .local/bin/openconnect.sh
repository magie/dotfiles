#!/bin/bash

COOKIE=
eval `openconnect --authenticate "$@"`
if [ -z "$COOKIE" ]; then
    exit 1
fi

sudo openconnect --servercert "$FINGERPRINT" "$CONNECT_URL" --cookie-on-stdin ${RESOLVE:+--resolve "$RESOLVE"} <<< "$COOKIE"
