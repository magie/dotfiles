#!/bin/bash

# Arbitrary but unique message tag
msgTag="mylight"

brightnessctl set "$@"

maxbright="$(brightnessctl m)"
brightness="$(echo $(brightnessctl g) $(brightnessctl m) | awk '{print ($1 / $2) * 100}' | sed 's/\.\w*//g')"

dunstify -a "changeLight" -u low -h string:x-dunst-stack-tag:$msgTag -h int:value:"$brightness" "Brightness: ${brightness}%"
