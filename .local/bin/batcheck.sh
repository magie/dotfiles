bat="/sys/class/power_supply/BAT0"
cap() {
    if [[ $capacity -le $1 && $oldcapacity -gt $1 ]]; then echo 1; else echo 0; fi
}
revcap() {
    if [[ $capacity -ge $1 && $oldcapacity -lt $1 ]]; then echo 1; else echo 0; fi
}

while true; do
    status=$(cat $bat/status)
    capacity=$(cat $bat/capacity)
    if [[ "$status" != "$oldstatus" || $(cap "5") -eq "1" || $(cap "20") -eq "1" || $(revcap "95") -eq "1" ]]; then
        if [[ "$status" == "Charging" ]]; then
            word="full"
        else
            word="remaining"
        fi
        dunstify -a "getBatteryState" -h string:x-dunst-stack-tag:myBatteryState -h int:value:"$capacity" "Battery is ${status,,}, $capacity% $word"
        oldstatus=$status
        oldcapacity=$capacity
    fi
    sleep 5
done
