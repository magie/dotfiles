#!/bin/bash

# Arbitrary but unique message tag
msgTag="myvolume"

if [[ "$@" == "toggle" ]]; then
    wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
else
    wpctl set-volume -l 1.5 @DEFAULT_SINK@ "$@"
fi

# Query amixer for the current volume and whether or not the speaker is muted
volume="$(wpctl get-volume @DEFAULT_SINK@ | awk '{print $2*100}')"

mute="$(wpctl get-volume @DEFAULT_SINK@ | awk '{print $3}')"
if [[ $volume == 0 || "$mute" == "[MUTED]" ]]; then
    # Show the sound muted notification
    dunstify -a "changeVolume" -u low -i audio-volume-muted -h string:x-dunst-stack-tag:$msgTag "Volume muted" 
else
    # Show the volume notification
    dunstify -a "changeVolume" -u low -i audio-volume-high -h string:x-dunst-stack-tag:$msgTag \
    -h int:value:"$volume" "Volume: ${volume}%"
fi
