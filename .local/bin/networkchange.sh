gdbus monitor -y -d net.connman.iwd |
    while read x; do
        info=$(iwctl station wlan0 show)
        state=$(echo "$info" | awk '/State/ {print $2}')
        case $state in
            connected)
                network=$(echo "$info" | awk '/Connected network/ {print $3}')
                message="Connected to network ${network}"
                ;;
            connecting)
                message="Connecting..."
                ;;
            disconnecting)
                message="Disconnecting..."
                ;;
            disconnected)
                message="Disconnected from network"
                ;;
            *)
                message="Weird network stuff - $state"
                ;;
        esac
        dunstify -a "changeNetworkState" -u low -h string:x-dunst-stack-tag:myNetworkState "$message"
    done
