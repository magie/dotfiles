#!/bin/bash
conky -c "/home/magnusr/.config/conky/conkymusicrc" &
conky -c "/home/magnusr/.config/conky/conkyprogressrc" &
glava --desktop &
lollypop
killall glava
kill $(pgrep -f 'conky -c /home/magnusr/.config/conky/conkymusicrc')
kill $(pgrep -f 'conky -c /home/magnusr/.config/conky/conkyprogressrc')
