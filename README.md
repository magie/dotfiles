Linux dotfiles.
Managed using git bare repo and submodules for plugins.

Setup:

This is hosted at `m4g13.xyz/dot.sh`

```bash
alias dot='/usr/bin/git --git-dir=$HOME/.local/share/dotbare/ --work-tree=$HOME'
git clone --bare git@gitlab.com:msandreereid/dotfiles.git $HOME/.local/share/dotbare/
dot checkout
dot submodule init
dot submodule update --remote
dot config --local status.showUntrackedFiles no
rm LICENSE README
dot update-index --skip-worktree README.md LICENSE
```